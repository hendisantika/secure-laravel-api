# Laravel Secure API APP

### Things to do list:
1. Clone this repository: `https://gitlab.com/hendisantika/secure-laravel-api.git`
2. Go inside the folder: `cd secure-laravel-api`
3. Run `cp .env.example .env` & set your desired db name
4. Run `composer install` 
5. Run `php artisan passport:install` 
6. Run `php artisan passport:keys` 
7. Run `php artisan key:generate` 
8. Run `php artisan migrate`
9. Run `php artisan db:seed`
10. Run `php artisan serve`

### Screen shot

Register New User

![Register New User](img/register.png "Register New User")

Login User

![Login User](img/login.png "Login User")

Access API

![Access API](img/access.png "Access API")
